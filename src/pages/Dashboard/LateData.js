import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
class LateData extends Component{
    state = {
        data: []
      };
      exportPDF = () => {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(15);
    
        const title = "My Awesome Report";
        const headers = [["PIN", "NAME", "DATE", "LATE"]];
    
        const data = this.state.data.map(elt=> [elt[0], elt[1], elt[2], elt[3]]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title, marginLeft, 40);
        doc.autoTable(content);
        doc.save("report.pdf")
      }
      componentDidMount() {
        fetch('http://localhost:8080/api/reportAttendance/getAllAttendance')  
        .then(res => res.json())
        .then((Data) => {
          this.setState({data : Data.Data })
        })
        .catch(console.log)
      }
      render() {
        const { data } = this.state;
        const options = {
          sizePerPage: 10,
          prePage: 'Previous',
          nextPage: 'Next',
          firstPage: 'First',
          lastPage: 'Last',
          hideSizePerPage: true,
          
        };
    
        
        console.log(data);
        return (
                <div className="card">
                  <div className="header">
                    <h4>Data Karyawan Terlambat</h4>
                    <p>React Bootstrap Table is a multi-features, powerful data table for React. Check it at here: <a href="http://allenfang.github.io/react-bootstrap-table/index.html" target="_blank">http://allenfang.github.io/react-bootstrap-table</a></p>
                  </div>
                  <div className="content">
                    <BootstrapTable      
                      data={data}
                      bordered={false}
                      striped
                      pagination={true}
                      options={options}>
                      <TableHeaderColumn
                        dataField='0'
                        isKey
                        width="15%"
                        dataSort>
                        User PIN
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField='1'
                        width="30%"
                        filter={ { type: 'TextFilter'} }
                        dataSort>
                        User Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField='2'
                        width="15%"
                        dataSort>
                        Date In
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField='3'
                        width="15%"
                        dataSort>
                        Total Late
                      </TableHeaderColumn>                 
                    </BootstrapTable>
                  </div>
                  <div>
                        <button onClick={() => this.exportPDF()}>Generate Report</button>
                    </div>
                </div>
                
    
        );
      }
    }
    export default LateData