import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import LateData from './LateData'
import PresentData from './PresentData'
import NotPresentData from './NotPresentData'
const Dashboard = () => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-md-12">
        <LateData />
      </div>
    </div>
     <div className="row">
      <div className="col-md-12">
        <NotPresentData />
      </div>
    </div>   
    <div className="row">
      <div className="col-md-12">
        <PresentData />
      </div>
    </div>
  </div>
);
export default Dashboard