import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

class NotPresent extends Component{
    state = {
        data: []
      };
    
      componentDidMount() {
        fetch('http://localhost:8080/api/reportAttendance/getAllAttendance')  
        .then(res => res.json())
        .then((Data) => {
          this.setState({data : Data.Data })
        })
        .catch(console.log)
      }
      render() {
        const { data } = this.state;
        const options = {
          sizePerPage: 10,
          prePage: 'Previous',
          nextPage: 'Next',
          firstPage: 'First',
          lastPage: 'Last',
          hideSizePerPage: true,
          
        };
    
        
        console.log(data);
        return (
          
                <div className="card">
                  <div className="header">
                    <h4>Data Karyawan Hadir</h4>
                    <p>React Bootstrap Table is a multi-features, powerful data table for React. Check it at here: <a href="http://allenfang.github.io/react-bootstrap-table/index.html" target="_blank">http://allenfang.github.io/react-bootstrap-table</a></p>
                  </div>
                  <div className="content">
                    <BootstrapTable      
                      data={data}
                      bordered={false}
                      striped
                      pagination={true}
                      options={options}>
                      <TableHeaderColumn
                        dataField='0'
                        isKey
                        width="15%"
                        dataSort>
                        User PIN
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField='1'
                        width="30%"
                        filter={ { type: 'TextFilter'} }
                        dataSort>
                        User Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField='2'
                        width="15%"
                        dataSort>
                        Date In
                      </TableHeaderColumn>
                                    
                    </BootstrapTable>
                  </div>
                </div>
    
        );
      }
    }
    export default NotPresent