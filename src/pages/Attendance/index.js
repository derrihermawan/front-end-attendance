import React from 'react';
import { Route } from 'react-router-dom';
import Late from './Late';
import Present from './Present';
import NotPresent from './Not Present';

const Tables = ({match}) => (
  <div className="content">
    <Route path={`${match.url}/late`} component={Late} />
    <Route path={`${match.url}/present`} component={Present} />
    <Route path={`${match.url}/notpresent`} component={NotPresent} />
  </div>
);

export default Tables;