import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Collapse } from 'react-bootstrap';


class Nav extends Component {

  state = {};

  render() {
    let { location } = this.props;
    return (
      
      <ul className="nav">
        
        <li className={location.pathname === '/' ? 'active' : null}>
          <Link to="/">
            <i className="pe-7s-news-paper"></i>
            <p>Dashboard</p>
          </Link>
        </li>
        <li className={this.isPathActive('/attendance') || this.state.tableMenuOpen ? 'active' : null}>
          <a onClick={() => this.setState({ tableMenuOpen: !this.state.tableMenuOpen })} data-toggle="collapse">
            <i className="pe-7s-note2"></i>
            <p>Attendance <b className="caret"></b></p>
          </a>
          <Collapse in={this.state.tableMenuOpen}>
            <div>
              <ul className="nav">
                <li className={this.isPathActive('/attendance/present') ? 'active' : null}>
                  <Link to="/attendance/present">Present</Link>
                </li>
                <li className={this.isPathActive('/attendance/late') ? 'active' : null}>
                  <Link to="/attendance/late">Come Late</Link>
                </li>
                <li className={this.isPathActive('/attendance/notpresent') ? 'active' : null}>
                  <Link to="/attendance/notpresent">Not Present</Link>
                </li>
              </ul>
            </div>
          </Collapse>
        </li>
        
        <li className={this.isPathActive('/employee') ? 'active' : null}>
          <Link to="/employee">
            <i className="pe-7s-users"></i>
            <p>Employee</p>
          </Link>
        </li>
        <li className={this.isPathActive('/recap') ? 'active' : null}>
          <Link to="/recap">
            <i className="pe-7s-news-paper"></i>
            <p>Attendance Recap</p>
          </Link>
        </li>
      </ul>
    );
  }

  isPathActive(path) {
    return this.props.location.pathname.startsWith(path);
  }
}

export default withRouter(Nav);